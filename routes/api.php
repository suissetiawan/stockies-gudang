<?php

Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::namespace('Barang')->middleware(['auth:api', 'role'])->group(function () {
    Route::post('new_barang', 'BarangController@store');
    Route::get('/barang/{id}', 'BarangController@show');
    Route::get('/barang', 'BarangController@index');
    Route::put('/barang/{id}', 'BarangController@update');
    Route::delete('/barang/{id}', 'BarangController@destroy');
});

Route::middleware(['auth:api', 'role'])->group(function () {
    Route::post('new_supplier', 'SupplierController@store');
    Route::get('/supplier/{kode}', 'SupplierController@show');
    Route::get('/supplier', 'SupplierController@index');
    Route::patch('edit-supplier/{kode}', 'SupplierController@update');
    Route::delete('hapus-supplier/{kode}', 'SupplierController@destroy');

    Route::get('/user', 'UserController@index');
    Route::get('/user/{id}', 'UserController@show');
    Route::patch('edit-user/{id}', 'UserController@update');
    Route::delete('hapus-user/{id}', 'UserController@destroy');
});

Route::namespace('Pembelian')->middleware('auth:api')->group(function () {
    Route::get('/list_pembelian', 'PembelianController@index');
    Route::post('new_pembelian', 'PembelianController@store');
    Route::get('/pembelian/{nota}', 'PembelianController@show');
    Route::delete('hapus-pembelian/{nota}', 'PembelianController@destroy');
    Route::post('/pembelian/{nota}/tambah_barang', 'DetailPembelianController@store');
    Route::delete('hapus-barang-nota/{id}', 'DetailPembelianController@destroy');
});

Route::get('user', 'UserController');
