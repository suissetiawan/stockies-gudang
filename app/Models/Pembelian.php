<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    protected $table = 'pembelian';
    protected $primaryKey = 'no_nota';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ['no_nota', 'kode_supplier', 'tanggal', 'total_pembelian', 'id_user'];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
}
