<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang';
    protected $primaryKey = 'kode_barang';
    public $incrementing = false;
    protected $fillable = [
        'kode_barang', 'nama_barang', 'stock', 'unit_pengukuran'
    ];
}
