<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'Supplier';
    protected $primaryKey = 'kode_supplier';
    public $incrementing = false;

    protected $fillable = [
        'kode_supplier', 'nama_supplier', 'no_telp', 'alamat'
    ];

    public function pembelian()
    {
        return $this->hasMany(Pembelian::class);
    }
}
