<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailPembelian extends Model
{
    protected $table = 'dtlpembelian';
    public $timestamps = false;

    protected $fillable = ['no_nota', 'kode_barang', 'qty', 'harga', 'total'];

    public function pembelian()
    {
        return $this->belongsTo(Pembelian::class);
    }
}
