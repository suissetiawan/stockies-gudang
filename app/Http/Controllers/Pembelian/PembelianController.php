<?php

namespace App\Http\Controllers\Pembelian;

use App\Http\Controllers\Controller;
use App\Models\DetailPembelian;
use App\Models\Pembelian;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pembelian = Pembelian::all();
        return $pembelian;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_nota' => ['required'],
            'kode_supplier' => ['required'],
        ]);

        $pembelian = Pembelian::create([
            'no_nota' => request('no_nota'),
            'kode_supplier' => request('kode_supplier'),
            'tanggal' => Carbon::now(),
            'id_user' => Auth::id(),
        ]);

        return response('transaksi berhasil dilakukan');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $nota
     * @return \Illuminate\Http\Response
     */
    public function show($nota)
    {
        $pembelian = Pembelian::where('no_nota', $nota)->first();
        $detail = DetailPembelian::where('no_nota', $nota)->get();
        return $pembelian . $detail;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tmp = $id;
        Pembelian::find($id)->delete();
        return response('data nota ' . $tmp . ' berhasil dihapus');
    }
}
