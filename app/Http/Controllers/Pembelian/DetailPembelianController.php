<?php

namespace App\Http\Controllers\Pembelian;

use App\Http\Controllers\Controller;
use App\Models\Barang;
use App\Models\DetailPembelian;
use App\Models\Pembelian;
use Illuminate\Http\Request;

class DetailPembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $nota)
    {
        $request->validate([
            'kode_barang' => ['required'],
            'qty' => ['required'],
            'harga' => ['required'],
        ]);
        //menjumlahkan harga
        $total = request('qty') * request('harga');

        //update stock
        $barang = Barang::where('kode_barang', request('kode_barang'))->first();
        $update_stok = $barang->stock - request('qty');
        Barang::where('kode_barang', request('kode_barang'))->update(['stock' => $update_stok]);

        $pembelian = Pembelian::where('no_nota', $nota)->first();
        $update_total = $pembelian->total_pembelian + $total;
        Pembelian::where('no_nota', $nota)->update(['total_pembelian' => $update_total]);

        $pembelian = DetailPembelian::create([
            'no_nota' => $nota,
            'kode_barang' => request('kode_barang'),
            'qty' => request('qty'),
            'harga' => request('harga'),
            'total' => $total,
        ]);

        return response('barang berhasil ditambahkan pada nota ' . $nota);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *     
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail = DetailPembelian::find($id)->first();
        $no_nota = $detail->no_nota;
        $kode_barang = $detail->kode_barang;

        $barang = Barang::where('kode_barang', $kode_barang)->first();
        $cur_stock = $barang->stock + $detail->qty;
        $barang = Barang::where('kode_barang', $kode_barang)->update(['stock' => $cur_stock]);

        $deleted = DetailPembelian::find($id)->delete();
        return response('data barang pada nota ' . $no_nota . ' yang memiliki kode barang ' . $kode_barang .
            ' berhasil dihapus');
    }
}
