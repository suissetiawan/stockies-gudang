<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;
use App\http\Resources\SupplierResource;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplier = Supplier::get();
        return SupplierResource::collection($supplier);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'kode_supplier' => ['required'],
            'nama_supplier' => ['required'],
            'no_telp' => ['required'],
            'alamat' => ['required'],
        ]);

        Supplier::create($this->SupplierStore());

        return response('supplier berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::where('kode_supplier', $id)->first();
        return new SupplierResource($supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        Supplier::find($id)->update($this->SupplierStore());
        return new SupplierResource(Supplier::find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Supplier::find($id)->delete();
        return response('supplier berhasil dihapus');
    }

    public function SupplierStore()
    {
        return [
            'kode_supplier' => Request('kode_supplier'),
            'nama_supplier' => request('nama_supplier'),
            'no_telp' => request('no_telp'),
            'alamat' => Request('alamat'),
        ];
    }
}
