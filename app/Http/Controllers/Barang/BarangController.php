<?php

namespace App\Http\Controllers\Barang;

use App\Models\Barang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BarangResource;
 
class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::get();
        return BarangResource::collection($barang);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_barang' => ['required'],
            'nama_barang' => ['required'],
            'stock' => ['required'],
            'unit_pengukuran' => ['required'],
        ]);

        $barang = Barang::create([
            'kode_barang' => request('kode_barang'),
            'nama_barang' => request('nama_barang'),
            'stock' => request('stock'),
            'unit_pengukuran' => request('unit_pengukuran'),
        ]);

        return $barang;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = Barang::where('kode_barang',$id)->first();
        return new BarangResource($barang);
        // return $barang;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = Barang::where('kode_barang',$id)->update([
            'nama_barang' => request('nama_barang'),
            'stock' => request('stock'),
            'unit_pengukuran' => request('unit_pengukuran'),
        ]);
        $barang = Barang::where('kode_barang',$id)->first();

        return new BarangResource($barang);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Barang::where('kode_barang',$id)->delete();

        return response()->json('Data Has Been Deleted',200);
    }
}
