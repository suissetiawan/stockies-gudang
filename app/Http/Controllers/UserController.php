<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\http\Resources\UserResource;

class UserController extends Controller
{
    
    public function __invoke(Request $request)
    {
        return $request->user()->name;
    }

    public function index()
    {
        $user = User::get();
        return UserResource::collection($user);
    }

    public function show($id)
    {
        $user = User::where('id',$id)->first();
        return new UserResource($user);
    }

    public function update(Request $request, $id)
    {
        User::find($id)->update([
            'name' => request('name'),
            'email' => request('email'),
            'role_user' => request('role_user'),
            'password' => bcrypt(request('password')),
        ]);
        return new UserResource(User::find($id));
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        return response('User berhasil dihapus');
    }
}
