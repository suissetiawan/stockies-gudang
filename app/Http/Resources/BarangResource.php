<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BarangResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'kode_barang' => $this->kode_barang,
            'nama_barang' => $this->nama_barang,
            'stock' => $this->stock,
            'unit_pengukuran' => $this->unit_pengukuran,
            'created_at' => $this->created_at->format('d F Y'),
            'updated_at' => $this->updated_at->format('d F Y'),
        ];
    }

    public function with($request){
        return ['status' => 'success'];
    }
}
